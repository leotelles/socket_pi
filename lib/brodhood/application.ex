defmodule Brodhood.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      BrodhoodWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Brodhood.PubSub},

      {
        Plug.Cowboy,
        scheme: :http,
        plug: {Plug.Parsers, parsers: [:json], pass: ["application/json"], json_decoder: Jason},
        options: [
          dispatch: dispatch(),
          port: 8080
        ]
      },


      Registry.child_spec( keys: :unique, name: Registry.People),

      # Start the Endpoint (http/https)
      BrodhoodWeb.Endpoint


      # Start a worker by calling: Brodhood.Worker.start_link(arg)
      # {Brodhood.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Brodhood.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    BrodhoodWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp dispatch do
    [
      {:_,
       [
         {"/[...]", BrodhoodWeb.Socket, %{}}
       ]}
    ]
  end

end

defmodule BrodhoodWeb.Disperse do
  use BrodhoodWeb, :controller


  def index(conn, %{"message" => message}) do

    return = %{response: "recebido"} |> Jason.encode!()
    BrodhoodWeb.Socket.brodcast(message)

    conn
    |> send_resp(200, return)
  end


end

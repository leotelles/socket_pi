defmodule BrodhoodWeb.Socket do
  @behaviour :cowboy_websocket
  require Logger

  def init(%{headers: headers} = req, state) do

    state = state |> Map.put(:registry_key,headers["sec-websocket-key"] )

    {:cowboy_websocket, req, state,
     %{
       :idle_timeout => :infinity,
       :inactivity_timeout => :infinity,
       :max_connections => :infinity
     }}
  end


  def websocket_init(state) do
    Registry.People
    |> Registry.register(state.registry_key, {})

    {:ok, state}
  end


  def websocket_handle({:text, json}, pid) do
    IO.puts("Recebendo mensagem do cliente.")

    {:reply, {:text, "pong"}, pid}
  end

  def websocket_info(info, state) do
    {:reply, {:text, info}, state}
  end

  def brodcast(msg) do

    Registry.select(Registry.People, [{{:"$1", :"$2", :"$3"}, [], [{{:"$1", :"$2", :"$3"}}]}])
    |> Stream.map(fn it ->  send_message(it,msg)  end)
    |> Enum.to_list()

  end

  def send_message({t, pid, _state},msg) do
    Process.send(pid, msg, [])
  end


end
